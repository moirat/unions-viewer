import React from 'react';
import '../index.css';

const issueTile = ( {issue} ) => {

    return (  
            <div className= "Rectangle">
            <div className ="row">
                <span className="Violation">{issue.violation}</span>
                <span className="ViolationType">{issue.violationType}</span>
            </div>

                <p className="Route">{issue.route}</p>
                
                <p className="Submitted">Submitted</p>  
                <p className="Time">{issue.submitTime}</p>
                
                <p className="LastViewed">Last Viewed</p>
                <p className="ViewerDetails">{issue.lastView}</p>
                
            <div className="row">   
                <p className="Views"> Num of Views</p>  
                <p className="NumViews">{issue.numViews}</p>
                
                <p className="Notes">Num of notes</p>  
                <p className="NumNotes">{issue.numNotes}</p>
            </div>
            </div>
    );

}

export default issueTile;
